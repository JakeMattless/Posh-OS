;GDT Code
gdt_start:

gdt_null: ;Mandatory null Descriptor
    dd 0x0 ;dd = define double (4 bytes)
    dd 0x0

gdt_code: ;Descriptor for code segment of GDT : For more info look at 'GDT.png'
          ;Set base=0x0, limit =0xfffff
          ;1st flags: (present)1 (privelege)00 (descriptor type)1 ->1001b
          ;Type flags: (code)1 (conforming)0 (readable)1 (accessed)0 -> 1010b
          ;2nd flags: (granularity)1 (32-bit default)1 (64-bit seg)0 (AVL)0 -> 1100b
    dw 0xffff   ;Limit (bits 0-15)
    dw 0x0      ;Base (bits 0-15)
    db 0x0      ;Base (bits 16-23)
    db 10011010b;1st flags, Type flags
    db 11001111b;2nd flags, limit (bits 16-19)
    db 0x0      ;Base (bits 24-31)

gdt_data: ;The data segment descriptor, Same as the code descriptor except type flags
          ;Type flags: (code)0 (expand down)0 (writable)1 (accessed)0 -> 0010b
    dw 0xffff   ;Limit (bits 0-15)
    dw 0x0      ;Base (bits 0-15)
    db 0x0      ;Base (bits 16-23)
    db 10010010b;1st flags, Type flags
    db 11001111b;2nd flags, limit (bits 16-19)
    db 0x0      ;Base (bits 24-31)   

gdt_end: ;The reason this is placed here is so that the assembler can calculate the
         ;Size of the GDT for the descriptor (below)

;GDT Descriptor
gdt_descriptor:
    dw gdt_end - gdt_start - 1 ; Get the size of the GDT, always -1 the actual size
    dd gdt_start               ; Start address of the GDT

;Define constants of the segment descriptor offsets
;They are what the segment registers have to contain when using protected mode.
;E.g. if DS = 0x10 in PM, the CPU knows to use the segment described at the offset
;0x10 (16 bytes) In the GDT, in this case it is the DATA segment
;(0x0 -> NULL; 0x08 -> CODE; 0x10 -> DATA)
CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start
