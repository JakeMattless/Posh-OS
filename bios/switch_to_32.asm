[bits 16]
switch_to_pm:
    cli ;Switch off interrupts until we have correctly set up protected mode
    lgdt [gdt_descriptor] ; Load the global descriptor table
                          ; Used to define the sebments in protected mode (e.g. code/data)

    mov eax, cr0          ; To switch to 32-bit Protected mode we must first set the first
    or eax, 0x1           ; bit of CR0, a control register. Note we can't set it directly
    mov cr0, eax          ; so we must first store it in the EAX register

    jmp CODE_SEG:init_pm  ; Make a 'far' jump, 'far' meaning to a new segment, to the 32-bit
                          ; code. This forces the CPU to flush its cache, full of
                          ;pre-fetched/real-mode instructions, that could cause issues

[bits 32] ;Tell the assembler to read this all as 32 bit instead of 16 bit
init_pm:
    mov ax, DATA_SEG ;Now within protected move, the old segments are useless
    mov ds, ax       ;point the old segment registers to our new selecter
    mov ss, ax       ;defined in the GDT
    mov es, ax
    mov fs, ax
    mov gs, ax

    mov ebp, 0x90000 ;Update stack position to the top of free space
    mov esp, ebp

    call BEGIN_PM ;Call a label defined in boot_sect.asm