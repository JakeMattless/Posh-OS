;A boot sector used to enter 32-bit protected mode.
[org 0x7c00]

    mov bp, 0x9000 ;Move the stack
    mov sp, bp

    mov bx, MSG_REAL_MODE
    call print_string

    call switch_to_pm ;The point of no return, we never come back here

    jmp $

%include "./print/print_string.asm"
%include "./print/print_string_32.asm"
%include "./bios/gdt.asm"
%include "./bios/switch_to_32.asm"

[bits 32] ;Everything from here on is 32 bits

;This is the arrival point after switching to/initialising 23-bit protected mode
BEGIN_PM:
    mov ebx, MSG_PROT_MODE
    call print_string_pm ;Use the new 32 bit printing routine

    jmp $

;Global Vars
MSG_REAL_MODE db "Started in 16-bit Real Mode", 0
MSG_PROT_MODE db "Successfully loaded into 32-bit Protected Mode", 0

;Padding for the boot sector
times 510-($-$$) db 0
dw 0xaa55