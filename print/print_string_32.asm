[bits 32]
;Define constant variables
VIDEO_MEMORY equ 0xb8000
WHITE_ON_BLACK equ 0x0f

;Print a null terminated string, pointer to by EDX
print_string_pm:
    pusha
    mov edx, VIDEO_MEMORY

print_string_pm_loop:
    mov al, [ebx] ; Store the char at the memory in EBX within AL
    mov ah, WHITE_ON_BLACK ; Store the attributes in AH

    cmp al, 0 ; if al == 0 (end of string) jump to done
    je done

    mov [edx], ax ;store the char and attributes, located at the current char cell
    add ebx, 1 ; increment ebx to the next char
    add edx, 2 ; Move to the next character cell within video memory

    jmp print_string_pm_loop ; Loop to print next char

done:
    popa
    ret
    