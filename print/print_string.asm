print_string:
    mov al, [bx] ; Go to the location in memory where the string in *bx* is stored
                 ; e.g. if *bx* is "Hello", it goes to the start of that string. This sets *al* to 'H' or 0x48
    cmp al, 0 ; Compare al to 0
    jne print_char ; If al != 0, jump to the "print_char" label
    ret ; Else return

print_char:
    mov ah, 0x0e ; Used for printing to the screen
    int 0x10 ; Print the value stored in al
    add bx, 0x0001 ; Increment the memory location stored in *bx* by 1 byte
    jmp print_string ; Jump back to the "print_function" label

print_new_line:
    mov ah, 0x0e
    mov al, 0x0d
    int 0x10
    mov al, 0x0a
    int 0x10
    ret
    